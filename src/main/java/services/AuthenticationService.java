package services;

import database.daos.UserDao;
import database.entities.User;
import endpoints.dtos.requests.LoginRequest;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.mindrot.jbcrypt.BCrypt;
import security.JWTManager;

@ApplicationScoped
public class AuthenticationService {
	@Inject
	UserDao userDao;

	public String login(LoginRequest credentials) throws LoginException {
		User user = userDao.getByLogin(credentials.login);

		if (user == null) {
			throw new LoginException("User not found");
		}

		if (!BCrypt.checkpw(credentials.password, user.hashedPassword)) {
			throw new LoginException("Password does not match the hash");
		}
		return JWTManager.generateToken(user);
	}

	public User getUserByLogin(String login) {
		return userDao.getByLogin(login);
	}

	public void createUser(String login, String email, String password, String securityQuestion, String securityAnswer) {
		User user = new User();
		user.email = email;
		user.login = login;
		user.securityQuestion = securityQuestion;
		user.securityAnswer = securityAnswer;
		String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt());
		user.hashedPassword = hashedPassword;
		userDao.insert(user);
	}

	public void updatePassword(String login, String newPassword) {
		String hashedPassword = BCrypt.hashpw(newPassword, BCrypt.gensalt());
		userDao.updatePassword(login, hashedPassword);
	}

	public boolean checkSecurityAnswer(User user, String answer) {
		return user.securityAnswer.equals(answer);
	}

	public static class LoginException extends Exception {
		LoginException(String message) {
			super(message);
		}
	}
}
