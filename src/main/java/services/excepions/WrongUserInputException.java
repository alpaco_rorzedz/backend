package services.excepions;

public class WrongUserInputException extends RuntimeException {
	public WrongUserInputException() {
	}

	public WrongUserInputException(String message) {
		super(message);
	}
}
