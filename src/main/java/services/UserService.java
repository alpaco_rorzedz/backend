package services;

import database.daos.CopyDao;
import database.daos.FavoriteDao;
import database.daos.LoanDao;
import database.daos.UserDao;
import database.entities.*;
import database.entities.enums.LoanStatusEnum;
import endpoints.dtos.requests.ChangeEmailRequest;
import endpoints.dtos.requests.ChangePasswordRequest;
import endpoints.dtos.responses.GetFavoritesForUserResponse;
import endpoints.dtos.responses.GetUserDetailsResponse;
import endpoints.dtos.responses.GetUserLoansResponse;
import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import org.apache.logging.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.joda.time.DateTime;
import org.mindrot.jbcrypt.BCrypt;
import services.excepions.WrongUserInputException;
import services.helpers.PaginationService;

@ApplicationScoped
public class UserService {
	@Inject
	UserDao userDao;
	@Inject
	LoanDao loanDao;
	@Inject
	CopyDao copyDao;
	@Inject
	FavoriteDao favoriteDao;
	@Inject
	PaginationService paginationService;
	@Context
	SecurityContext securityContext;
	@Inject
	Logger logger;

	public GetUserDetailsResponse getUserDetails() {
		User authUser = (User) securityContext.getUserPrincipal();
		User user = userDao.get(authUser.id);
		return new GetUserDetailsResponse(user.login, user.email);
	}

	public GetUserLoansResponse getUserLoans() {
		User authUser = (User) securityContext.getUserPrincipal();
		List<Loan> loans = loanDao.getByUserId(authUser.id);

		return new GetUserLoansResponse(
				loans.stream().map(it -> new GetUserLoansResponse.Data(
						it.id,
						it.copy.description.title,
						it.created.toString(),
						it.returnDate.toString(),
						it.loanState.ordinal()
				)).collect(Collectors.toList())
		);
	}

	public void changeEmail(ChangeEmailRequest request) {
		long authUserId = ((User) securityContext.getUserPrincipal()).id;

		User user = userDao.get(authUserId);

		if (user.email.equals(request.oldEmail)) {
			user.email = request.newEmail;
			userDao.update(user);
		} else {
			throw new WrongUserInputException("Emails does not match");
		}
	}

	public void changePassword(ChangePasswordRequest request) {
		long authUserId = ((User) securityContext.getUserPrincipal()).id;

		User user = userDao.get(authUserId);

		if (BCrypt.checkpw(request.oldPassword, user.hashedPassword)) {
			user.hashedPassword = BCrypt.hashpw(request.newPassword, BCrypt.gensalt());
			userDao.update(user);
		} else {
			throw new WrongUserInputException("Passwords does not match");
		}
	}

	public void reserveCopy(long descriptionId) {
		long authUserId = ((User) securityContext.getUserPrincipal()).id;
		Copy copy = copyDao.getFirstAvailable(descriptionId);

		if (copy == null) {
			throw new WrongUserInputException("No available copy");
		}
		logger.info(copy.id);

		copy.isAvailable = false;

		loanDao.insert(new Loan(
				copy,
				new User(authUserId),
				new Date(DateTime.now().plusDays(7).getMillis()),
				LoanStatusEnum.RESERVED
		));
		copyDao.update(copy);
	}

	public void deleteReservation(long loanId) {
		long authUserId = ((User) securityContext.getUserPrincipal()).id;

		Loan loan = loanDao.get(loanId);

		if (loan == null) {
			throw new WrongUserInputException("Loan does not exist");
		}

		if (loan.user.id != authUserId) {
			throw new WrongUserInputException("Loan does not belong to current user");
		}

		loan.copy.isAvailable = true;

		copyDao.update(loan.copy);
		loanDao.delete(loanId);
	}

	public GetFavoritesForUserResponse getFavorites() {
		long authUserId = ((User) securityContext.getUserPrincipal()).id;

		List<Favorite> favorites = favoriteDao.getByUserId(authUserId);

		return new GetFavoritesForUserResponse(
				favorites.stream().map(it -> new GetFavoritesForUserResponse.Data(
						it.id,
						it.description.id,
						it.description.title
				)).collect(Collectors.toList())
		);
	}

	public void addFavorite(long descriptionId) {
		User authUser = (User) securityContext.getUserPrincipal();

		Favorite favorite = new Favorite(
				authUser,
				new Description(descriptionId)
		);

		try {
			favoriteDao.insert(favorite);
		} catch (Exception e) {
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new WrongUserInputException("Description is already a favorite");
			}
			throw e;
		}
	}

	public void deleteFavorite(long favoriteId) {
		User authUser = (User) securityContext.getUserPrincipal();

		Favorite favorite = favoriteDao.get(favoriteId);

		if (favorite == null) {
			throw new WrongUserInputException("Favorite doesn't exist");
		}

		if (favorite.user.id != authUser.id) {
			throw new WrongUserInputException("Favorite doesn't belong to current user");
		}

		favoriteDao.delete(favoriteId);
	}
}
