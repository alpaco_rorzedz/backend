package services;

import database.daos.GenreDao;
import endpoints.dtos.responses.GetAllGenresResponse;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class GenreService {
	@Inject
	GenreDao genreDao;

	public GetAllGenresResponse getAllGenres() {
		return new GetAllGenresResponse(genreDao.getAll());
	}
}
