package services;

import database.daos.PublisherDao;
import database.entities.Publisher;
import endpoints.dtos.responses.GetAllPublishersResponse;
import java.util.List;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import services.helpers.PaginationService;

@ApplicationScoped
public class PublisherService {
	@Inject
	PublisherDao publisherDao;

	public GetAllPublishersResponse getAllPublishers(int page) {
		List<Publisher> publishers = publisherDao.getAll();

		PaginationService.Pagination<Publisher> pagination = new PaginationService().paginate(publishers, page);

		List<GetAllPublishersResponse.Data> items = pagination.paginatedItems.stream().map(it -> new GetAllPublishersResponse.Data(
				it.id,
				it.name
		)).collect(Collectors.toList());

		return new GetAllPublishersResponse(
				pagination.totalItems,
				pagination.perPage,
				pagination.lastPage,
				items
		);
	}
}
