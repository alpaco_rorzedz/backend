package services;

import database.daos.CommentDao;
import database.entities.Comment;
import database.entities.Description;
import database.entities.User;
import endpoints.dtos.requests.PostCommentRequest;
import endpoints.dtos.responses.GetUserCommentsResponse;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import services.excepions.WrongUserInputException;

@ApplicationScoped
public class CommentService {
	@Inject
	CommentDao commentDao;
	@Context
	SecurityContext securityContext;

	public GetUserCommentsResponse getUserComments(long userId) {
		return new GetUserCommentsResponse(commentDao.getByUserId(userId).stream().map(it -> it.content).collect(Collectors.toList()));
	}

	public void postComment(PostCommentRequest request) {
		long authUserId = ((User) securityContext.getUserPrincipal()).id;

		Comment comment = new Comment(
				null,
				new User(authUserId),
				false,
				request.content,
				new Description(request.descriptionId)
		);
		commentDao.insert(comment);
	}

	public void deleteComment(long commentId) {
		long authUserId = ((User) securityContext.getUserPrincipal()).id;
		Comment commentToDelete = commentDao.get(commentId);

		if (commentToDelete == null) {
			throw new WrongUserInputException("Comment does not exist");
		}

		if (commentToDelete.user.id != authUserId) {
			throw new WrongUserInputException("Comment does not belong to current user");
		}
		commentDao.delete(commentId);
	}
}
