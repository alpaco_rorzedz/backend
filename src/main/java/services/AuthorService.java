package services;

import database.daos.AuthorDao;
import database.daos.DescriptionDao;
import database.entities.Author;
import database.entities.Description;
import endpoints.dtos.responses.AuthorDetailsResponse;
import endpoints.dtos.responses.GetAllAuthorsResponse;
import java.util.List;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import services.helpers.PaginationService;

@ApplicationScoped
public class AuthorService {
	@Inject
	AuthorDao authorDao;
	@Inject
	DescriptionDao descriptionDao;
	@Inject
	PaginationService paginationService;

	public GetAllAuthorsResponse getAllAuthors(int page) {
		List<Author> authors = authorDao.getAll();

		PaginationService.Pagination<Author> pagination = paginationService.paginate(authors, page);

		List<GetAllAuthorsResponse.Data> items = pagination.paginatedItems
				.stream()
				.map(it -> new GetAllAuthorsResponse.Data(it.id, it.firstName, it.lastName))
				.collect(Collectors.toList());

		return new GetAllAuthorsResponse(pagination.totalItems, pagination.perPage, pagination.lastPage, items);
	}

	public AuthorDetailsResponse getAuthorDetails(long authorId) {
		List<Description> descriptions = descriptionDao.getByAuthorId(authorId);
		Author author = authorDao.get(authorId);

		if (author == null) {
			return null;
		}

		return new AuthorDetailsResponse(
				author.id,
				author.firstName,
				author.lastName,
				author.biography,
				descriptions
						.stream()
						.map(it -> new AuthorDetailsResponse.Book(it.id, it.title))
						.collect(Collectors.toList())
		);
	}
}
