package services;

import database.daos.*;
import database.entities.Loan;
import database.entities.User;
import database.entities.enums.LoanStatusEnum;
import endpoints.dtos.responses.AdminLoansResponse;
import endpoints.dtos.responses.GetAdminUserDetailsResponse;
import endpoints.dtos.responses.GetUsersResponse;
import java.util.List;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import services.excepions.WrongUserInputException;
import services.helpers.PaginationService;

@ApplicationScoped
public class AdminService {
	@Inject
	DescriptionDao descriptionDao;
	@Inject
	UserDao userDao;
	@Inject
	LoanDao loanDao;
	@Inject
	CopyDao copyDao;
	@Inject
	PaginationService paginationService;
	@Inject
	RatingDao ratingDao;
	@Context
	SecurityContext securityContext;

	public GetUsersResponse getUsers() {
		List<GetUsersResponse.Data> users = userDao.getAll().stream().map(it -> new GetUsersResponse.Data(
				it.id,
				it.login
		)).collect(Collectors.toList());
		PaginationService.Pagination<GetUsersResponse.Data> pagination = paginationService.paginate(users, 1);
		return new GetUsersResponse(pagination, pagination.paginatedItems);
	}

	public GetAdminUserDetailsResponse getUserDetails(long userId) {
		User user = userDao.get(userId);
		List<GetAdminUserDetailsResponse.Loan> loans = loanDao.getByUserId(userId).stream().map(it -> new GetAdminUserDetailsResponse.Loan(
				it.id,
				it.copy.description.title,
				it.returnDate.toString(),
				it.created.toString(),
				it.loanState
		)).collect(Collectors.toList());

		return new GetAdminUserDetailsResponse(
				new GetAdminUserDetailsResponse.Data(user.login, loans)
		);
	}

	public AdminLoansResponse getLoans() {
		List<Loan> loans = loanDao.getAll();
		return new AdminLoansResponse(loans);
	}

	public void changeLoanStatus(long loanId, int loanStatus) {
		Loan loan = loanDao.get(loanId);

		if (loan == null) {
			throw new WrongUserInputException("Loan does not exist");
		}

		LoanStatusEnum newStatus = LoanStatusEnum.get(loanStatus);

		loan.copy.isAvailable = newStatus != LoanStatusEnum.RESERVED && newStatus != LoanStatusEnum.LOANED;
		loan.loanState = newStatus;

		copyDao.update(loan.copy);
		loanDao.update(loan);
	}
}
