package services;

import database.daos.CommentDao;
import database.daos.CopyDao;
import database.daos.DescriptionDao;
import database.daos.RatingDao;
import database.entities.Description;
import database.entities.Rating;
import database.entities.User;
import endpoints.dtos.requests.GetBooksRequest;
import endpoints.dtos.requests.SearchBookRequest;
import endpoints.dtos.responses.GetBookDetailsResponse;
import endpoints.dtos.responses.GetBooksResponse;
import endpoints.dtos.responses.SearchBookResponse;
import java.util.List;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import services.excepions.ItemNotFoundException;
import services.helpers.PaginationService;

@ApplicationScoped
public class BookService {
	@Inject
	DescriptionDao descriptionDao;
	@Inject
	CommentDao commentDao;
	@Inject
	CopyDao copyDao;
	@Inject
	PaginationService paginationService;
	@Inject
	RatingDao ratingDao;
	@Context
	SecurityContext securityContext;

	public GetBooksResponse getBooks(GetBooksRequest request) {
		GetBooksRequest.Filters filters = request.filters;
		List<Description> descriptions = descriptionDao.getFiltered(
				filters.authorId,
				filters.genreId,
				filters.publisherId
		);

		PaginationService.Pagination<Description> pagination = paginationService.paginate(descriptions, request.page);

		List<GetBooksResponse.Data> items = pagination.paginatedItems
				.stream()
				.map(it -> new GetBooksResponse.Data(
						it.id,
						it.author.id,
						it.title,
						it.author.firstName + " " + it.author.lastName,
						it.publisher.name,
						it.genre.name,
						likesToRating(it.likes)))
				.collect(Collectors.toList());

		return new GetBooksResponse(pagination.totalItems, pagination.perPage, pagination.lastPage, items);
	}

	private double likesToRating(List<Rating> likes) {
		long likesCount = likes.stream().filter(it -> !it.isNegative).count();
		return (double) likesCount / likes.size() * 100.0;
	}

	public GetBookDetailsResponse getBookDetails(long descriptionId) throws ItemNotFoundException {
		Description description = descriptionDao.get(descriptionId);

		if (description == null) {
			throw new ItemNotFoundException();
		}

		List<GetBookDetailsResponse.Comment> comments = commentDao.getByDescriptionId(descriptionId)
				.stream()
				.map(it ->
						new GetBookDetailsResponse.Comment(
								it.id,
								it.user.login,
								it.content
						))
				.collect(Collectors.toList());

		String authorFullName = description.author.firstName + " " + description.author.lastName;

		Integer userLike = null;
		User authUser = ((User) securityContext.getUserPrincipal());

		if (authUser != null) {
			Rating userRating = description.likes.stream().filter(it -> it.user.id == authUser.id).findFirst().orElse(null);
			if (userRating != null) {
				userLike = getRatingEnum(userRating);
			}
		}

		long availableCopies = copyDao.getByDescriptionId(descriptionId).stream().filter(it -> it.isAvailable).count();

		return new GetBookDetailsResponse(
				description.id,
				description.title,
				authorFullName,
				description.publisher.name,
				description.genre.name,
				likesToRating(description.likes),
				availableCopies,
				comments,
				userLike
		);
	}

	private int getRatingEnum(Rating rating) {
		int userLike;
		if (rating.isNegative) {
			userLike = -1;
		} else {
			userLike = 1;
		}
		return userLike;
	}

	public void rateBook(long descriptionId, boolean isNegative) {
		User authUser = ((User) securityContext.getUserPrincipal());
		if (authUser == null) {
			throw new RuntimeException("User not authenticated");
		}
		ratingDao.insert(new Rating(
				null,
				new Description(descriptionId),
				new User(authUser.id),
				isNegative
		));
	}

	public SearchBookResponse searchBooks(SearchBookRequest request) {
		List<Description> descriptions = descriptionDao.getByTitle(request.query);

		List<GetBooksResponse.Data> items = descriptions
				.stream()
				.map(it -> new GetBooksResponse.Data(
						it.id,
						it.author.id,
						it.title,
						it.author.firstName + " " + it.author.lastName,
						it.publisher.name,
						it.genre.name,
						likesToRating(it.likes)))
				.collect(Collectors.toList());

		PaginationService.Pagination<GetBooksResponse.Data> pagination = paginationService.paginate(items, request.page);

		return new SearchBookResponse(pagination.paginatedItems, pagination);
	}
}
