package services.helpers;

import java.util.Collections;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.json.bind.annotation.JsonbTransient;

@ApplicationScoped
public class PaginationService {

	private int itemsPerPage = 10;

	public <T> Pagination<T> paginate(List<T> itemsToPaginate, int page) {
		int dbPage = page - 1;
		int total = itemsToPaginate.size();

		if (dbPage == -1) {
			return new Pagination<T>(itemsToPaginate, total, null, null);
		}

		int pagingStartIndex = itemsPerPage * dbPage;
		int pagingEndIndex = itemsPerPage * (dbPage + 1);
		int lastPage = total / itemsPerPage + 1;

		if (pagingStartIndex > total) {
			return new Pagination<T>(Collections.emptyList(), total, lastPage, itemsPerPage);
		}

		if (pagingEndIndex > total - 1) {
			pagingEndIndex -= pagingEndIndex - total;
		}

		List<T> paginatedItems = itemsToPaginate.subList(pagingStartIndex, pagingEndIndex);

		return new Pagination<T>(paginatedItems, total, lastPage, itemsPerPage);
	}

	public static class Pagination<T> {
		@JsonbTransient
		public final List<T> paginatedItems;
		public final Integer totalItems;
		public final Integer lastPage;
		public final Integer perPage;

		public Pagination(List<T> paginatedItems, Integer totalItems, Integer lastPage, Integer perPage) {
			this.paginatedItems = paginatedItems;
			this.totalItems = totalItems;
			this.lastPage = lastPage;
			this.perPage = perPage;
		}
	}
}
