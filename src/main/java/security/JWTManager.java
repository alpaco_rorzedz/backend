package security;

import database.entities.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.joda.time.DateTime;

public class JWTManager {
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String CLAIM_KEY_LOGIN = "LOGIN";
	public static final String CLAIM_KEY_ID = "USER_ID";
	public static final String CLAIM_KEY_ROLE = "ROLE";
	public static final String ROLE_ADMIN = "ADMIN";
	public static final String ROLE_USER = "USER";
	private static final String TOKEN_SECRET = "fTjWnZq4t7w!z%C*F-JaNdRgUkXp2s5u8x/A?D(G+KbPeShVmY";

	public static String generateToken(User user) {
		return Jwts.builder()
				.setSubject("users")
				.claim(CLAIM_KEY_ID, user.id)
				.claim(CLAIM_KEY_LOGIN, user.login)
				.claim(CLAIM_KEY_ROLE, user.isAdmin ? ROLE_ADMIN : ROLE_USER)
				.setIssuedAt(DateTime.now().toDate())
				.setExpiration(DateTime.now().plusYears(1).toDate())
				.signWith(SignatureAlgorithm.HS512, TOKEN_SECRET)
				.compact();
	}

	public static Claims checkToken(String token) {
		return Jwts.parser().setSigningKey(TOKEN_SECRET).parseClaimsJws(token).getBody();
	}
}
