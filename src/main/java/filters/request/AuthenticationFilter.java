package filters.request;

import database.entities.User;
import io.jsonwebtoken.Claims;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Priority;
import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import org.apache.logging.log4j.Logger;
import security.JWTCheck;

import static security.JWTManager.*;

@Provider
@Priority(Priorities.AUTHENTICATION)
@Dependent
public class AuthenticationFilter implements ContainerRequestFilter {
	@Context
	private ResourceInfo resourceInfo;
	@Inject
	Logger logger;

	private List<Class<? extends Annotation>> authorizationAnnotations = Arrays.asList(
			DenyAll.class,
			RolesAllowed.class,
			PermitAll.class
	);

	@Override
	public void filter(ContainerRequestContext requestContext) {
		try {
			String token = findToken(requestContext);
			Claims claims = checkToken(token);

			SecurityContext currentSecurityContext = requestContext.getSecurityContext();

			JWTSecurityContext newSecurityContext = new JWTSecurityContext(
					claims.get(CLAIM_KEY_LOGIN).toString(),
					Long.parseLong(claims.get(CLAIM_KEY_ID).toString()),
					claims.get(CLAIM_KEY_ROLE).toString(),
					currentSecurityContext.isSecure(),
					currentSecurityContext.getAuthenticationScheme()
			);
			requestContext.setSecurityContext(newSecurityContext);
		} catch (Exception e) {
			if (hasAuthenticationAnnotation() || hasAnyAuthorizationAnnotations()) {
				requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
			}
		}
	}

	private String findToken(ContainerRequestContext containerRequestContext) {
		String header = containerRequestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
		if (header != null && header.startsWith(TOKEN_PREFIX)) {
			return header.substring(TOKEN_PREFIX.length());
		}
		return null;
	}

	private boolean hasAuthenticationAnnotation() {
		return resourceInfo.getResourceMethod().isAnnotationPresent(JWTCheck.class);
	}

	private boolean hasAnyAuthorizationAnnotations() {
		Method method = resourceInfo.getResourceMethod();
		Class<?> klass = resourceInfo.getResourceClass();
		return authorizationAnnotations.stream().anyMatch(method::isAnnotationPresent) ||
				authorizationAnnotations.stream().anyMatch(klass::isAnnotationPresent);
	}

	public static class JWTSecurityContext implements SecurityContext {
		private final String login;
		private final long userId;
		private final String role;
		private final boolean isSecure;
		private final String authenticationScheme;

		public JWTSecurityContext(String login, long userId, String role, boolean isSecure, String authenticationScheme) {
			this.login = login;
			this.userId = userId;
			this.role = role;
			this.isSecure = isSecure;
			this.authenticationScheme = authenticationScheme;
		}

		@Override
		public Principal getUserPrincipal() {
			return new User(userId, login);
		}

		@Override
		public boolean isUserInRole(String role) {
			return role.equals(this.role);
		}

		@Override
		public boolean isSecure() {
			return isSecure;
		}

		@Override
		public String getAuthenticationScheme() {
			return authenticationScheme;
		}
	}
}
