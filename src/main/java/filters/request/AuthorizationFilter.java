package filters.request;

import java.lang.reflect.Method;
import java.util.Arrays;
import javax.annotation.Priority;
import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
@Priority(Priorities.AUTHORIZATION)
public class AuthorizationFilter implements ContainerRequestFilter {
	@Context
	private ResourceInfo resourceInfo;

	@Override
	public void filter(ContainerRequestContext requestContext) {
		Method method = resourceInfo.getResourceMethod();

		if (method.isAnnotationPresent(DenyAll.class)) {
			rejectRequest(requestContext);
		}

		RolesAllowed rolesAllowed = method.getAnnotation(RolesAllowed.class);

		if (rolesAllowed != null) {
			checkPermissions(rolesAllowed.value(), requestContext);
			return;
		}

		if (method.isAnnotationPresent(PermitAll.class)) {
			return;
		}

		rolesAllowed = resourceInfo.getResourceClass().getAnnotation(RolesAllowed.class);
		if (rolesAllowed != null) {
			checkPermissions(rolesAllowed.value(), requestContext);
		}
	}

	private void rejectRequest(ContainerRequestContext requestContext) {
		requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
	}

	private void checkPermissions(String[] rolesAllowed, ContainerRequestContext requestContext) {
		if (requestContext.getSecurityContext().getUserPrincipal() == null) {
			rejectRequest(requestContext);
		}

		boolean isAllowed = Arrays.stream(rolesAllowed).anyMatch(role ->
				requestContext.getSecurityContext().isUserInRole(role)
		);

		if (!isAllowed) {
			rejectRequest(requestContext);
		}
	}
}
