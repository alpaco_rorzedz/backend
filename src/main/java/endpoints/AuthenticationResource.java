package endpoints;

import database.entities.User;
import endpoints.dtos.requests.LoginRequest;
import endpoints.dtos.requests.RegisterRequest;
import endpoints.dtos.requests.RestorePasswordRequest;
import endpoints.dtos.responses.ErrorResponse;
import endpoints.dtos.responses.LoginResponse;
import endpoints.dtos.responses.SecurityQuestionResponse;
import endpoints.helpers.ValidationChecker;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.Logger;
import services.AuthenticationService;

@Path("auth")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AuthenticationResource {
	@Inject
	AuthenticationService authenticationService;
	@Inject
	Logger logger;
	@Inject
	ValidationChecker validationChecker;

	@POST
	@Path("login")
	public Response login(LoginRequest request) {
		try {
			String token = authenticationService.login(request);
			return Response.ok(new LoginResponse(token)).build();
		} catch (AuthenticationService.LoginException e) {
			logger.debug(e.getMessage());
			e.printStackTrace();
			return Response.status(Response.Status.FORBIDDEN).build();
		}
	}

	@POST
	@Path("register")
	public Response register(RegisterRequest request) {
		if (!validationChecker.validate(request)) {
			logger.info("Wrong parameters");
			return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorResponse("Wrong parameters")).build();
		}

		if (authenticationService.getUserByLogin(request.login) != null) {
			logger.info("User already exists");
			return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorResponse("User already exists")).build();
		}

		authenticationService.createUser(
				request.login,
				request.email,
				request.password,
				request.securityQuestion,
				request.securityAnswer
		);

		return Response.ok().build();
	}

	@POST
	@Path("restore")
	public Response restorePassword(RestorePasswordRequest request) {
		if (!validationChecker.validate(request)) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

		User user = authenticationService.getUserByLogin(request.login);

		if (user == null) {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.entity(new ErrorResponse("User does not exist"))
					.build();
		}

		if (!authenticationService.checkSecurityAnswer(user, request.securityAnswer)) {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.entity(new ErrorResponse("Security answer is incorrect"))
					.build();
		}

		authenticationService.updatePassword(request.login, request.newPassword);

		return Response.ok().build();
	}

	@GET
	@Path("question/{login}")
	public Response getSecurityQuestion(@PathParam("login") String login) {
		User user = authenticationService.getUserByLogin(login);

		if (user == null) {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.entity(new ErrorResponse("User does not exist"))
					.build();
		}

		return Response.ok(new SecurityQuestionResponse(user.securityQuestion)).build();
	}
}
