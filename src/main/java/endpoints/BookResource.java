package endpoints;

import endpoints.dtos.requests.GetBooksRequest;
import endpoints.dtos.requests.RateBookRequest;
import endpoints.dtos.requests.SearchBookRequest;
import endpoints.helpers.ValidationChecker;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.Logger;
import security.JWTCheck;
import services.BookService;
import services.excepions.ItemNotFoundException;

@Path("books")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BookResource {
	@Inject
	BookService bookService;
	@Inject
	Logger logger;
	@Inject
	ValidationChecker validationChecker;

	@GET
	@Path("")
	public Response getBooks(GetBooksRequest request) {
		return Response.ok(bookService.getBooks(request)).build();
	}

	@GET
	@Path("search")
	public Response searchBooks(SearchBookRequest request) {
		return Response.ok(bookService.searchBooks(request)).build();
	}

	@GET
	@Path("details/{bookId}")
	public Response getBookDetails(@PathParam("bookId") long bookId) {
		try {
			return Response.ok(bookService.getBookDetails(bookId)).build();
		} catch (ItemNotFoundException e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@POST
	@JWTCheck
	@Path("rate")
	public Response rateBook(RateBookRequest request) {
		try {
			bookService.rateBook(request.descriptionId, request.isNegative);
			return Response.ok().build();
		} catch (Exception e) {
			logger.info(e);
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
}
