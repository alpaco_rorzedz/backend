package endpoints;

import endpoints.dtos.requests.PostCommentRequest;
import endpoints.helpers.ValidationChecker;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.Logger;
import security.JWTCheck;
import services.CommentService;
import services.excepions.WrongUserInputException;

@Path("comments")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CommentResource {
	@Inject
	CommentService commentService;
	@Inject
	Logger logger;
	@Inject
	ValidationChecker validationChecker;

	@GET
	@Path("user/{userId}")
	public Response getUserComments(@PathParam("userId") long userId) {
		return Response.ok(commentService.getUserComments(userId)).build();
	}

	@POST
	@JWTCheck
	public Response postComment(PostCommentRequest request) {
		commentService.postComment(request);
		return Response.ok().build();
	}

	@DELETE
	@JWTCheck
	@Path("{commentId}")
	public Response deleteComment(@PathParam("commentId") long commentId) {
		try {
			commentService.deleteComment(commentId);
		} catch (WrongUserInputException e) {
			logger.info(e.getMessage());
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok().build();
	}
}
