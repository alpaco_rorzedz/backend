package endpoints;

import endpoints.dtos.requests.AdminLoanStatusRequest;
import endpoints.helpers.ValidationChecker;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.Logger;
import security.JWTManager;
import services.AdminService;
import services.excepions.WrongUserInputException;

@Path("admin")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RolesAllowed(JWTManager.ROLE_ADMIN)
public class AdminResource {
	@Inject
	AdminService adminService;
	@Inject
	Logger logger;
	@Inject
	ValidationChecker validationChecker;

	@GET
	@Path("users")
	public Response getUsers() {
		return Response.ok(adminService.getUsers()).build();
	}

	@GET
	@Path("userdetails/{userId}")
	public Response getUserDetails(@PathParam("userId") long userId) {
		return Response.ok(adminService.getUserDetails(userId)).build();
	}

	@GET
	@Path("loans")
	public Response getLoans() {
		return Response.ok(adminService.getLoans()).build();
	}

	@PATCH
	@Path("loans/changeStatus")
	public Response changeLoanStatus(AdminLoanStatusRequest request) {
		try {
			adminService.changeLoanStatus(request.loanId, request.loanStatus);
			return Response.ok().build();
		} catch (WrongUserInputException e) {
			logger.error(e);
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
}
