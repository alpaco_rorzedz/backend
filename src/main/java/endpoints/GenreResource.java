package endpoints;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import services.GenreService;
import services.excepions.ItemNotFoundException;

@Path("genres")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class GenreResource {
	@Inject
	GenreService genreService;

	@GET
	public Response getAllGenres() {
		try {
			return Response.ok(genreService.getAllGenres()).build();
		} catch (ItemNotFoundException e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
}
