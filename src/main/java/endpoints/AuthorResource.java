package endpoints;

import endpoints.helpers.ValidationChecker;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.Logger;
import services.AuthorService;

@Path("authors")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AuthorResource {
	@Inject
	AuthorService authorService;
	@Inject
	Logger logger;
	@Inject
	ValidationChecker validationChecker;

	@GET
	@Path("/{page}")
	public Response getAllAuthors(@PathParam("page") int page) {
		return Response.ok(authorService.getAllAuthors(page)).build();
	}

	@GET
	@Path("/details/{authorId}")
	public Response getAuthorDetails(@PathParam("authorId") long authorId) {
		return Response.ok(authorService.getAuthorDetails(authorId)).build();
	}
}
