package endpoints.dtos.responses;

import database.entities.Genre;
import java.util.List;

public class GetAllGenresResponse {
	public List<Genre> data;

	public GetAllGenresResponse(List<Genre> data) {
		this.data = data;
	}
}
