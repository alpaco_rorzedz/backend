package endpoints.dtos.responses;

import java.util.List;
import services.helpers.PaginationService;

public class GetUsersResponse {
	public PaginationService.Pagination<Data> pagination;
	public List<Data> data;

	public GetUsersResponse(PaginationService.Pagination<Data> pagination, List<Data> data) {
		this.pagination = pagination;
		this.data = data;
	}

	public static class Data {
		public long id;
		public String name;

		public Data(long id, String name) {
			this.id = id;
			this.name = name;
		}
	}
}
