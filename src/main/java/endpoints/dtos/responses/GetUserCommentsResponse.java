package endpoints.dtos.responses;

import java.util.List;

public class GetUserCommentsResponse {
	public List<String> data;

	public GetUserCommentsResponse(List<String> data) {
		this.data = data;
	}
}
