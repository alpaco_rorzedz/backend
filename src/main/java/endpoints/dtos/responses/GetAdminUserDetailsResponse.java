package endpoints.dtos.responses;

import database.entities.enums.LoanStatusEnum;
import java.util.List;

public class GetAdminUserDetailsResponse {
	public Data data;

	public GetAdminUserDetailsResponse(Data data) {
		this.data = data;
	}

	public static class Data {
		public String username;
		public List<Loan> loans;

		public Data(String username, List<Loan> loans) {
			this.username = username;
			this.loans = loans;
		}
	}

	public static class Loan {
		public long id;
		public String title;
		public String returnDate, reserveDate;
		public LoanStatusEnum loanState;

		public Loan(long id, String title, String returnDate, String reserveDate, LoanStatusEnum loanState) {
			this.id = id;
			this.title = title;
			this.returnDate = returnDate;
			this.reserveDate = reserveDate;
			this.loanState = loanState;
		}
	}
}
