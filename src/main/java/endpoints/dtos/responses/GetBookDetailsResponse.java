package endpoints.dtos.responses;

import java.util.List;

public class GetBookDetailsResponse {
	public long bookId;
	public String title, author, publisher, genre;
	public double rating;
	public long availableCopyCount;
	public List<Comment> comments;
	public Integer userLike;

	public GetBookDetailsResponse(long bookId, String title, String author, String publisher, String genre, double rating, long availableCopyCount, List<Comment> comments, Integer userLike) {
		this.bookId = bookId;
		this.title = title;
		this.author = author;
		this.publisher = publisher;
		this.genre = genre;
		this.rating = rating;
		this.availableCopyCount = availableCopyCount;
		this.comments = comments;
		this.userLike = userLike;
	}

	public static class Comment {
		public long id;
		public String login, content;

		public Comment(long id, String login, String content) {
			this.id = id;
			this.login = login;
			this.content = content;
		}
	}
}
