package endpoints.dtos.responses;

import java.util.List;

public class GetUserLoansResponse {
	public List<Data> data;

	public GetUserLoansResponse(List<Data> data) {
		this.data = data;
	}

	public static class Data {
		public Long id;
		public String title;
		public String createdAt;
		public String returnDate;
		public Integer state;

		public Data(Long id, String title, String createdAt, String returnDate, Integer state) {
			this.id = id;
			this.title = title;
			this.createdAt = createdAt;
			this.returnDate = returnDate;
			this.state = state;
		}
	}
}
