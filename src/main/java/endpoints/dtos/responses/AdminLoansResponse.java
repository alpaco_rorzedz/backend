package endpoints.dtos.responses;

import database.entities.Loan;
import java.util.List;
import java.util.stream.Collectors;

public class AdminLoansResponse {
	public List<Data> data;

	public AdminLoansResponse(List<Loan> loans) {
		data = loans.stream().map(it -> new Data(
				it.id,
				it.user.id,
				it.copy.id,
				it.copy.description.title,
				it.loanState.ordinal()
		)).collect(Collectors.toList());
	}

	public static class Data {
		public Long id;
		public Long userId;
		public Long copyId;
		public String title;
		public Integer loanState;

		public Data(Long id, Long userId, Long copyId, String title, Integer loanState) {
			this.id = id;
			this.userId = userId;
			this.copyId = copyId;
			this.title = title;
			this.loanState = loanState;
		}
	}
}
