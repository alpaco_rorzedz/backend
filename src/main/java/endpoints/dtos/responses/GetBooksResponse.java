package endpoints.dtos.responses;

import java.util.List;

public class GetBooksResponse {
	public long total;
	public Integer perPage;
	public Integer lastPage;
	public List<Data> data;

	public GetBooksResponse(long total, Integer perPage, Integer lastPage, List<Data> data) {
		this.total = total;
		this.perPage = perPage;
		this.lastPage = lastPage;
		this.data = data;
	}

	public static class Data {
		public long bookId;
		public String title, author, publisher;
		public String genre;
		public double rating;
		public long authorId;

		public Data(long bookId, long authorId, String title, String author, String publisher, String genre, double rating) {
			this.bookId = bookId;
			this.authorId = authorId;
			this.title = title;
			this.author = author;
			this.publisher = publisher;
			this.genre = genre;
			this.rating = rating;
		}
	}
}
