package endpoints.dtos.responses;

import java.util.List;

public class GetAllPublishersResponse {
	public long total;
	public Integer perPage;
	public Integer lastPage;
	public List<Data> data;

	public GetAllPublishersResponse(long total, Integer perPage, Integer lastPage, List<Data> data) {
		this.total = total;
		this.perPage = perPage;
		this.lastPage = lastPage;
		this.data = data;
	}

	public static class Data {
		public long id;
		public String name;

		public Data(long id, String name) {
			this.id = id;
			this.name = name;
		}
	}
}
