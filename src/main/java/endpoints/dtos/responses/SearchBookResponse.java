package endpoints.dtos.responses;

import java.util.List;
import services.helpers.PaginationService;

public class SearchBookResponse {

	public List<GetBooksResponse.Data> data;
	public PaginationService.Pagination<GetBooksResponse.Data> pagination;

	public SearchBookResponse(List<GetBooksResponse.Data> data, PaginationService.Pagination<GetBooksResponse.Data> pagination) {
		this.data = data;
		this.pagination = pagination;
	}
}
