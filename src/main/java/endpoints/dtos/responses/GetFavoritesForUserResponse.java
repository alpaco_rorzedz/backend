package endpoints.dtos.responses;

import java.util.List;

public class GetFavoritesForUserResponse {
	public List<Data> data;

	public GetFavoritesForUserResponse(List<Data> data) {
		this.data = data;
	}

	public static class Data {
		public long id, descriptionId;
		public String title;

		public Data(long id, long descriptionId, String title) {
			this.id = id;
			this.descriptionId = descriptionId;
			this.title = title;
		}
	}
}
