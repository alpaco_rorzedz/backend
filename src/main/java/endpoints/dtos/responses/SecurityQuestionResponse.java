package endpoints.dtos.responses;

public class SecurityQuestionResponse {
	public String securityQuestion;

	public SecurityQuestionResponse(String securityQuestion) {
		this.securityQuestion = securityQuestion;
	}
}
