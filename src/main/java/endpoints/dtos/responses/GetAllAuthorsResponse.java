package endpoints.dtos.responses;

import java.util.List;

public class GetAllAuthorsResponse {
	public long total;
	public Integer perPage;
	public Integer lastPage;
	public List<Data> data;

	public GetAllAuthorsResponse(long total, Integer perPage, Integer lastPage, List<Data> data) {
		this.total = total;
		this.perPage = perPage;
		this.lastPage = lastPage;
		this.data = data;
	}

	public static class Data {
		public long id;
		public String firstName;
		public String lastName;

		public Data(long id, String firstName, String lastName) {
			this.id = id;
			this.firstName = firstName;
			this.lastName = lastName;
		}
	}
}
