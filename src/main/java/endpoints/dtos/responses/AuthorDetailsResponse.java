package endpoints.dtos.responses;

import java.util.List;

public class AuthorDetailsResponse {
	public long id;
	public String firstName;
	public String lastName;
	public String biography;
	public List<Book> books;

	public AuthorDetailsResponse(long id, String firstName, String lastName, String biography, List<Book> books) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.biography = biography;
		this.books = books;
	}

	public static class Book {
		public long id;

		public String title;

		public Book(long id, String title) {
			this.id = id;
			this.title = title;
		}
	}
}
