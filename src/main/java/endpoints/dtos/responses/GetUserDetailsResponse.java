package endpoints.dtos.responses;

public class GetUserDetailsResponse {
	public String login;
	public String email;

	public GetUserDetailsResponse(String login, String email) {
		this.login = login;
		this.email = email;
	}
}
