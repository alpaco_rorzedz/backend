package endpoints.dtos.requests;

public class RestorePasswordRequest {
	public String login;
	public String securityAnswer;
	public String newPassword;
}
