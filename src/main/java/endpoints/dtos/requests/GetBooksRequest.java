package endpoints.dtos.requests;

public class GetBooksRequest {
	public int page;
	public Filters filters;

	public static class Filters {
		public Long authorId, publisherId, genreId;
	}
}
