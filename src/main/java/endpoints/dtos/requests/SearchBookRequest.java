package endpoints.dtos.requests;

public class SearchBookRequest {
	public String query;
	public Integer page;
}
