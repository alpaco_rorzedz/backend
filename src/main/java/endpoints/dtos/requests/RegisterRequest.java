package endpoints.dtos.requests;

import javax.validation.constraints.NotBlank;

public class RegisterRequest {
	@NotBlank
	public String login;
	@NotBlank
	public String email;
	@NotBlank
	public String password;
	@NotBlank
	public String securityQuestion;
	@NotBlank
	public String securityAnswer;
}
