package endpoints.dtos.requests;

public class AdminLoanStatusRequest {
	public Long loanId;
	public Integer loanStatus;
}
