package endpoints.helpers;

import java.util.Set;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import org.apache.logging.log4j.Logger;

@ApplicationScoped
public class ValidationChecker {
	@Inject
	Validator validator;
	@Inject
	Logger logger;

	public <T> Boolean validate(T request) {
		Set<ConstraintViolation<T>> violations = validator.validate(request);
		if (violations.isEmpty()) {
			return true;
		}
		logger.debug(violations.toString());
		return false;
	}
}
