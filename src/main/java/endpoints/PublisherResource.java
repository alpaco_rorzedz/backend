package endpoints;

import endpoints.helpers.ValidationChecker;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.Logger;
import services.PublisherService;

@Path("publishers")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PublisherResource {
	@Inject
	PublisherService publisherService;
	@Inject
	Logger logger;
	@Inject
	ValidationChecker validationChecker;

	@GET
	@Path("{page}")
	public Response getAllPublishers(@PathParam("page") int page) {
		return Response.ok(publisherService.getAllPublishers(page)).build();
	}
}
