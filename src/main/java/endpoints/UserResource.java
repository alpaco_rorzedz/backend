package endpoints;

import endpoints.dtos.requests.ChangeEmailRequest;
import endpoints.dtos.requests.ChangePasswordRequest;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.Logger;
import security.JWTCheck;
import services.UserService;
import services.excepions.WrongUserInputException;

@Path("user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@JWTCheck
public class UserResource {
	@Inject
	UserService userService;
	@Inject
	Logger logger;

	@GET
	@Path("details/{userId}")
	public Response getUserDetails(@PathParam("userId") long userId) {
		return Response.ok(userService.getUserDetails()).build();
	}

	@GET
	@Path("loans")
	public Response getUserLoans() {
		return Response.ok(userService.getUserLoans()).build();
	}

	@PATCH
	@Path("changeEmail")
	public Response changeEmail(ChangeEmailRequest request) {
		try {
			userService.changeEmail(request);
			return Response.ok().build();
		} catch (WrongUserInputException e) {
			logger.info(e);
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@PATCH
	@Path("changePassword")
	public Response changePassword(ChangePasswordRequest request) {
		try {
			userService.changePassword(request);
			return Response.ok().build();
		} catch (WrongUserInputException e) {
			logger.info(e);
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@POST
	@Path("reservation/{descriptionId}")
	public Response reserveCopy(@PathParam("descriptionId") long descriptionId) {
		try {
			userService.reserveCopy(descriptionId);
			return Response.ok().build();
		} catch (WrongUserInputException e) {
			logger.info(e);
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@DELETE
	@Path("reservation/{loanId}")
	public Response deleteReservation(@PathParam("loanId") long loanId) {
		try {
			userService.deleteReservation(loanId);
			return Response.ok().build();
		} catch (WrongUserInputException e) {
			logger.info(e);
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@GET
	@Path("favorites")
	public Response getFavorites() {
		return Response.ok(userService.getFavorites()).build();
	}

	@POST
	@Path("favorites/{descriptionId}")
	public Response addFavorite(@PathParam("descriptionId") long descriptionId) {
		try {
			userService.addFavorite(descriptionId);
			return Response.ok().build();
		} catch (WrongUserInputException e) {
			logger.info(e);
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@DELETE
	@Path("favorites/{favoriteId}")
	public Response deleteFavorite(@PathParam("favoriteId") long favoriteId) {
		try {
			userService.deleteFavorite(favoriteId);
			return Response.ok().build();
		} catch (WrongUserInputException e) {
			logger.info(e);
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
}
