package database.entities;

import javax.persistence.*;

@Entity
@Table(name = "ratings", schema = "biblioteka")
public class Rating {
	@Id
	@GeneratedValue
	public Long id;

	@JoinColumn(name = "description_id")
	@ManyToOne(fetch = FetchType.EAGER)
	public Description description;

	@JoinColumn(name = "user_id")
	@ManyToOne(fetch = FetchType.EAGER)
	public User user;

	@Column(name = "is_negative")
	public boolean isNegative;

	public Rating() {
	}

	public Rating(Long id, Description description, User user, boolean isNegative) {
		this.id = id;
		this.description = description;
		this.user = user;
		this.isNegative = isNegative;
	}
}
