package database.entities;

import java.security.Principal;
import javax.persistence.*;
import org.hibernate.annotations.NaturalId;

@Entity
@Table(name = "users", schema = "biblioteka")
public class User implements Principal {
	@Id
	@GeneratedValue
	@Column(name = "id")
	public long id;
	@Column(name = "login")
	@NaturalId
	public String login;
	@Column(name = "password")
	public String hashedPassword;
	@Column(name = "email")
	public String email;
	@Column(name = "is_admin")
	public boolean isAdmin;
	@Column(name = "security_question")
	public String securityQuestion;
	@Column(name = "security_answer")
	public String securityAnswer;

	public User() {
	}

	public User(long id) {
		this.id = id;
	}

	public User(long id, String login) {
		this.id = id;
		this.login = login;
	}

	public User(String login, String hashedPassword, String email, boolean isAdmin, String securityQuestion, String securityAnswer) {
		this.login = login;
		this.hashedPassword = hashedPassword;
		this.email = email;
		this.isAdmin = isAdmin;
		this.securityQuestion = securityQuestion;
		this.securityAnswer = securityAnswer;
	}

	@Override
	public String getName() {
		return login;
	}
}
