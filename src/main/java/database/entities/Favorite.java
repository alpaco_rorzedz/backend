package database.entities;

import javax.persistence.*;

@Entity
@Table(name = "favorites", schema = "biblioteka")
public class Favorite {
	@Id
	@GeneratedValue
	public long id;

	@JoinColumn(name = "user_id")
	@ManyToOne(fetch = FetchType.EAGER)
	public User user;

	@JoinColumn(name = "description_id")
	@ManyToOne(fetch = FetchType.EAGER)
	public Description description;


	public Favorite() {
	}

	public Favorite(User user, Description description) {
		this.user = user;
		this.description = description;
	}
}
