package database.entities;

import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "descriptions", schema = "biblioteka")
public class Description {
	@Id
	@GeneratedValue
	public long id;

	@Column(name = "title")
	public String title;

	@JoinColumn(name = "author_id")
	@ManyToOne(fetch = FetchType.EAGER)
	public Author author;

	@JoinColumn(name = "genre_id")
	@ManyToOne(fetch = FetchType.EAGER)
	public Genre genre;

	@JoinColumn(name = "publisher_id")
	@ManyToOne(fetch = FetchType.EAGER)
	public Publisher publisher;

	@OneToMany(mappedBy = "description", fetch = FetchType.EAGER)
	public List<Rating> likes;

	@Column(name = "state")
	public int state;

	public Description() {
	}

	public Description(long id) {
		this.id = id;
	}
}
