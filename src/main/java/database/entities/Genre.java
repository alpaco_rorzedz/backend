package database.entities;

import javax.persistence.*;

@Entity
@Table(name = "genres", schema = "biblioteka")
public class Genre {
	@Id
	@GeneratedValue
	public long id;
	@Column(name = "name")
	public String name;
}
