package database.entities;

import javax.persistence.*;

@Entity
@Table(name = "comments", schema = "biblioteka")
public class Comment {
	@Id
	@GeneratedValue
	public Long id;

	@ManyToOne
	@JoinColumn(name = "user_id")
	public User user;

	@Column(name = "is_approved")
	public boolean isApproved;

	@Column(name = "content")
	public String content;

	@ManyToOne()
	@JoinColumn(name = "description_id")
	public Description description;

	public Comment() {
	}

	public Comment(Long id, User user, boolean isApproved, String content, Description description) {
		this.id = id;
		this.user = user;
		this.isApproved = isApproved;
		this.content = content;
		this.description = description;
	}
}
