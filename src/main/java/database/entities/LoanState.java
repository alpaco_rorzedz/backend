package database.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "loan_states", schema = "biblioteka")
public class LoanState {

	@Id
	public long id;
	@Column(name = "name")
	public String name;
}
