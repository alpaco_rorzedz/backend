package database.entities;

import javax.persistence.*;

@Entity
@Table(name = "copies", schema = "biblioteka")
public class Copy {
	@Id
	@GeneratedValue
	public long id;
	@JoinColumn(name = "description_id")
	@ManyToOne
	public Description description;
	@Column(name = "is_damaged")
	public boolean isDamaged;
	@Column(name = "is_available")
	public boolean isAvailable;

	public Copy() {
	}

	public Copy(long id) {
		this.id = id;
	}
}
