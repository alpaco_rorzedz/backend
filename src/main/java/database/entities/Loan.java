package database.entities;

import database.entities.enums.LoanStatusEnum;
import java.sql.Date;
import java.sql.Timestamp;
import javax.persistence.*;

@Entity
@Table(name = "loans", schema = "biblioteka")
public class Loan {
	@Id
	@GeneratedValue
	public long id;

	@JoinColumn(name = "copy_id")
	@ManyToOne
	public Copy copy;

	@JoinColumn(name = "user_id")
	@ManyToOne
	public User user;

	@Column(name = "created")
	public Timestamp created;

	@Column(name = "return_date")
	public Date returnDate;

	@Column(name = "loan_state_id")
	@Enumerated
	public LoanStatusEnum loanState;

	public Loan() {
	}

	public Loan(Copy copy, User user, Date returnDate, LoanStatusEnum loanState) {
		this.copy = copy;
		this.user = user;
		this.returnDate = returnDate;
		this.loanState = loanState;
	}
}
