package database.entities.enums;

public enum LoanStatusEnum {
	RESERVED, //0
	LOANED, //1
	RETURNED; //2

	public static LoanStatusEnum get(int value) {
		switch (value) {
			case 0:
				return LoanStatusEnum.RESERVED;
			case 1:
				return LoanStatusEnum.LOANED;
			case 2:
				return LoanStatusEnum.RETURNED;
		}
		return null;
	}
}
