package database.entities;

import javax.persistence.*;

@Entity
@Table(name = "publishers", schema = "biblioteka")
public class Publisher {
	@Id
	@GeneratedValue
	public long id;
	@Column(name = "name")
	public String name;

}
