package database.entities;

import javax.persistence.*;

@Entity()
@Table(name = "authors", schema = "biblioteka")
public class Author {
	@Id
	@GeneratedValue
	public long id;
	@Column(name = "first_name")
	public String firstName;
	@Column(name = "last_name")
	public String lastName;
	@Column(name = "biography")
	public String biography;
}
