package database.daos;

import database.entities.Copy;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import org.hibernate.Session;
import org.hibernate.Transaction;

@ApplicationScoped
public class CopyDao extends AbstractDao<Copy> {

	public CopyDao() {
		super(Copy.class);
	}

	public List<Copy> getByDescriptionId(long descriptionId) {
		Session s = getSession();
		Transaction tx = s.beginTransaction();

		List<Copy> result = s.createQuery("from Copy WHERE description.id = :descriptionId", Copy.class)
				.setParameter("descriptionId", descriptionId)
				.list();

		tx.commit();
		return result;
	}

	public Copy getFirstAvailable(long descriptionId) {
		Session s = getSession();
		Transaction tx = s.beginTransaction();

		List<Copy> result = s.createQuery("from Copy WHERE description.id = :descriptionId AND isAvailable = true", Copy.class)
				.setParameter("descriptionId", descriptionId)
				.list();

		tx.commit();

		if (result.size() == 0) {
			return null;
		}
		return result.get(0);
	}
}