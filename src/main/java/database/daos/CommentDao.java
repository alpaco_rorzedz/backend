package database.daos;

import database.entities.Comment;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import org.hibernate.Session;
import org.hibernate.Transaction;

@ApplicationScoped
public class CommentDao extends AbstractDao<Comment> {

	public CommentDao() {
		super(Comment.class);
	}

	public List<Comment> getByDescriptionId(long descriptionId) {
		Session s = getSession();
		Transaction tx = s.beginTransaction();

		List<Comment> result = s.createQuery("from Comment WHERE description.id = :descriptionId", Comment.class)
				.setParameter("descriptionId", descriptionId)
				.list();

		tx.commit();
		return result;
	}

	public List<Comment> getByUserId(long userId) {
		Session s = getSession();
		Transaction tx = s.beginTransaction();

		List<Comment> result = s.createQuery("from Comment WHERE user.id = :userId", Comment.class)
				.setParameter("userId", userId)
				.list();

		tx.commit();
		return result;
	}
}