package database.daos;

import database.entities.Description;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.criteria.*;
import org.hibernate.Session;
import org.hibernate.Transaction;

@ApplicationScoped
public class DescriptionDao extends AbstractDao<Description> {

	public DescriptionDao() {
		super(Description.class);
	}

	public List<Description> getByAuthorId(long authorId) {
		Session s = getSession();
		Transaction tx = s.beginTransaction();

		List<Description> result = s.
				createQuery("from Description d INNER JOIN FETCH d.author a WHERE a.id = :authorId", Description.class)
				.setParameter("authorId", authorId)
				.list();

		tx.commit();
		return result;
	}

	public List<Description> getFiltered(Long authorId, Long genreId, Long publisherId) {
		Session s = getSession();
		Transaction tx = s.beginTransaction();

		CriteriaBuilder builder = s.getCriteriaBuilder();
		CriteriaQuery<Description> cq = builder.createQuery(Description.class);
		Root<Description> root = cq.from(Description.class);
		root.fetch("genre", JoinType.INNER);
		root.fetch("author", JoinType.INNER);
		root.fetch("publisher", JoinType.INNER);

		List<Predicate> predicates = new ArrayList<>();

		if (authorId != null)
			predicates.add(builder.equal(root.get("author").get("id"), authorId));
		if (genreId != null)
			predicates.add(builder.equal(root.get("genre").get("id"), genreId));
		if (publisherId != null)
			predicates.add(builder.equal(root.get("publisher").get("id"), publisherId));

		cq.where(predicates.toArray(new Predicate[]{}));

		List<Description> result = s.createQuery(cq).list();

		tx.commit();
		return result;
	}

	public List<Description> getByTitle(String title) {
		Session s = getSession();
		Transaction tx = s.beginTransaction();

		List<Description> result = s.createQuery("from Description WHERE title LIKE :title", Description.class)
				.setParameter("title", "%" + title + "%")
				.list();

		tx.commit();
		return result;
	}
}