package database.daos;

import database.entities.Loan;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import org.hibernate.Session;
import org.hibernate.Transaction;

@ApplicationScoped
public class LoanDao extends AbstractDao<Loan> {

	public LoanDao() {
		super(Loan.class);
	}

	public List<Loan> getByUserId(long userId) {
		Session s = getSession();
		Transaction tx = s.beginTransaction();

		List<Loan> result = s.
				createQuery("from Loan WHERE user.id = :userId", Loan.class)
				.setParameter("userId", userId)
				.list();

		tx.commit();
		return result;
	}

/*	public List<Loan> getReservedByUserId(long userId) {
		Session s = getSession();
		Transaction tx = s.beginTransaction();

		List<Loan> result = s.createQuery("from Loan WHERE userId = :userId AND loanStatus = :loanStatus", Loan.class)
				.setParameter("userId", userId)
				.setParameter("loanStatus", LoanStateEnum.RESERVED)
				.list();

		tx.commit();
		return result;
	}*/
}