package database.daos;

import database.entities.Favorite;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import org.hibernate.Session;
import org.hibernate.Transaction;

@ApplicationScoped
public class FavoriteDao extends AbstractDao<Favorite> {

	public FavoriteDao() {
		super(Favorite.class);
	}

	public List<Favorite> getByUserId(long userId) {
		Session s = getSession();
		Transaction tx = s.beginTransaction();

		List<Favorite> result = s.
				createQuery("from Favorite WHERE user.id = :userId", Favorite.class)
				.setParameter("userId", userId)
				.list();

		tx.commit();
		return result;
	}
}