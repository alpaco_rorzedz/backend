package database.daos;

import database.entities.User;
import javax.enterprise.context.ApplicationScoped;
import org.hibernate.Session;
import org.hibernate.Transaction;

@ApplicationScoped
public class UserDao extends AbstractDao<User> {
	public UserDao() {
		super(User.class);
	}

	public void updateEmail(long id, String email) {
		Session s = getSession();
		Transaction tx = s.beginTransaction();

		User user = get(id);
		user.email = email;
		s.update(user);

		tx.commit();
	}

	public void updatePassword(String login, String newHashedPassword) {
		Session s = getSession();
		Transaction tx = s.beginTransaction();

		s.createQuery("UPDATE User SET password = :newHashedPassword WHERE login = :login")
				.setParameter("login", login)
				.setParameter("newHashedPassword", newHashedPassword)
				.executeUpdate();

		tx.commit();
	}

	public User getByLogin(String login) {
		Session s = getSession();
		Transaction tx = s.beginTransaction();

		User result = s.createQuery("from User WHERE login = :login", User.class)
				.setParameter("login", login)
				.uniqueResult();

		tx.commit();
		return result;
	}
}