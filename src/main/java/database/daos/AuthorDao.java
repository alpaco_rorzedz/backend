package database.daos;

import database.entities.Author;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class AuthorDao extends AbstractDao<Author> {

	public AuthorDao() {
		super(Author.class);
	}

}