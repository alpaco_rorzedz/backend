package database.daos;

import database.entities.Genre;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class GenreDao extends AbstractDao<Genre> {

	public GenreDao() {
		super(Genre.class);
	}

}