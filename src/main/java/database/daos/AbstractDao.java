package database.daos;

import database.helpers.HibernateUtil;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

abstract class AbstractDao<T> {
	private Class<T> clazz;

	AbstractDao(Class<T> entityClass) {
		clazz = entityClass;
	}

	public Session getSession() {
		return HibernateUtil.getSessionFactory().getCurrentSession();
	}

	public T get(long id) {
		Session s = getSession();
		Transaction tx = s.beginTransaction();

		T result = getSession().find(clazz, id);

		tx.commit();
		return result;
	}

	public List<T> getAll() {
		Session s = getSession();
		Transaction tx = s.beginTransaction();
		List<T> result = getSession().createQuery("from " + clazz.getName(), clazz).getResultList();

		tx.commit();
		return result;
	}

	public void insert(T obj) {
		Session s = getSession();
		Transaction tx = s.beginTransaction();
		s.save(obj);
		tx.commit();
	}

	public void delete(long id) {
		Session s = getSession();
		Transaction tx = s.beginTransaction();

		s.createQuery("DELETE from " + clazz.getName() + " WHERE id = :id")
				.setParameter("id", id)
				.executeUpdate();

		tx.commit();
	}

	public void update(T obj) {
		Session s = getSession();
		Transaction tx = s.beginTransaction();
		s.update(obj);
		tx.commit();
	}
}
