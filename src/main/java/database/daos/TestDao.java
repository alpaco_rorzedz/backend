package database.daos;

import database.entities.Comment;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import org.hibernate.Session;
import org.hibernate.query.Query;

@ApplicationScoped
public class TestDao extends AbstractDao<Comment> {
	public TestDao() {
		super(Comment.class);
	}

	public List<Comment> getCustom() {
		Session s = getSession();
		Query<Comment> query = s.createQuery("from Comment c JOIN FETCH c.description JOIN FETCH c.user", Comment.class);
		return query.getResultList();
	}
}