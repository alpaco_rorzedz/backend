package database.daos;

import database.entities.Publisher;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PublisherDao extends AbstractDao<Publisher> {

	public PublisherDao() {
		super(Publisher.class);
	}
}