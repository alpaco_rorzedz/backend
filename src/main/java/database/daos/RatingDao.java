package database.daos;

import database.entities.Rating;
import javax.enterprise.context.ApplicationScoped;
import org.hibernate.Session;
import org.hibernate.Transaction;

@ApplicationScoped
public class RatingDao extends AbstractDao<Rating> {

	public RatingDao() {
		super(Rating.class);
	}

	public Rating getByDescriptionAndUserId(long descriptionId, long userId) {
		Session s = getSession();
		Transaction tx = s.beginTransaction();

		Rating result = s.createQuery("from Rating WHERE description.id = :descriptionId AND user.id = :userId", Rating.class)
				.setParameter("descriptionId", descriptionId)
				.setParameter("userId", userId)
				.uniqueResult();

		tx.commit();
		return result;
	}
}