package producers;

import com.google.gson.Gson;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

@ApplicationScoped
public class GsonProducer {
	private Gson gson;

	@Produces
	public Gson produceGson() {
		if (gson == null) {
			gson = new Gson();
		}
		return gson;
	}
}
