-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 25 Gru 2019, 17:49
-- Wersja serwera: 10.1.35-MariaDB
-- Wersja PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `biblioteka`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `authors`
--

CREATE TABLE `authors` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `biography` longtext COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `authors`
--

INSERT INTO `authors` (`id`, `first_name`, `last_name`, `biography`) VALUES
(1, 'Bolesław', 'Prus', 'Polski pisarz, prozaik, nowelista i publicysta okresu pozytywizmu, współtwórca polskiego realizmu, kronikarz Warszawy, myśliciel i popularyzator wiedzy, działacz społeczny, propagator turystyki pieszej i rowerowej. Jeden z najwybitniejszych i najważniejszych pisarzy w historii literatury polskiej'),
(2, 'Jan', 'Paweł II', 'Polski duchowny rzymskokatolicki, biskup pomocniczy krakowski (1958–1964), a następnie arcybiskup metropolita krakowski (1964–1978), kardynał (1967–1978), Zastępca Przewodniczącego Konferencji Episkopatu Polski (1969–1978), 264. papież i 6. Suweren Państwa Watykańskiego w latach 1978–2005. Święty Kościoła katolickiego.\r\n\r\nPoeta i poliglota, a także aktor, dramaturg i pedagog. Filozof historii, fenomenolog, mistyk i przedstawiciel personalizmu chrześcijańskiego.'),
(3, 'Stefan', 'Żeromski', 'Polski prozaik, publicysta, dramaturg; pierwszy prezes polskiego PEN Clubu. Czterokrotnie nominowany do Nagrody Nobla w dziedzinie literatury (1921, 1922, 1923, 1924). Jeden z najwybitniejszych pisarzy polskich w historii['),
(4, 'Czesław', 'Miłosz', 'Polski poeta, prozaik, eseista, historyk literatury, tłumacz, dyplomata; w latach 1951–1993 przebywał na emigracji, do 1960 we Francji, następnie w Stanach Zjednoczonych; w Polsce do 1980 obłożony cenzurą; laureat Nagrody Nobla w dziedzinie literatury (1980); profesor Uniwersytetu Kalifornijskiego w Berkeley i Uniwersytetu Harvarda; pochowany w Krypcie Zasłużonych na Skałce; brat Andrzeja Miłosza. Uznawany za najwybitniejszego polskiego poetę XX wieku'),
(5, 'Adam', 'Mickiewicz', 'Polski poeta, działacz polityczny, publicysta, tłumacz, filozof, działacz religijny, mistyk, organizator i dowódca wojskowy, nauczyciel akademicki.'),
(6, 'Juliusz', 'Słowacki', 'Polski poeta, przedstawiciel romantyzmu, dramaturg i epistolograf. Obok Mickiewicza i Krasińskiego określany jako jeden z Wieszczów Narodowych. Twórca filozofii genezyjskiej (pneumatycznej), epizodycznie związany także z mesjanizmem polskim, był też mistykiem. Obok Mickiewicza uznawany powszechnie za największego przedstawiciela polskiego romantyzmu.'),
(7, 'Henryk', 'Sienkiewicz', 'Polski nowelista, powieściopisarz i publicysta; laureat Nagrody Nobla w dziedzinie literatury (1905) za całokształt twórczości[1], jeden z najpopularniejszych polskich pisarzy przełomu XIX i XX w');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_approved` bit(1) NOT NULL,
  `content` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `description_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `is_approved`, `content`, `description_id`) VALUES
(1, 1, b'0', 'Siemka', 1),
(2, 1, b'1', 'co tam?', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `copies`
--

CREATE TABLE `copies` (
  `id` int(11) NOT NULL,
  `description_id` int(11) NOT NULL,
  `is_damaged` bit(1) NOT NULL DEFAULT b'0',
  `copy_state_id` int(11) DEFAULT NULL,
  `is_available` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `copies`
--

INSERT INTO `copies` (`id`, `description_id`, `is_damaged`, `copy_state_id`, `is_available`) VALUES
(6, 1, b'0', 0, b'0'),
(7, 1, b'0', 0, b'0'),
(8, 1, b'0', 0, b'0'),
(9, 1, b'1', 0, b'0'),
(10, 1, b'0', 0, b'0'),
(11, 1, b'0', 0, b'0'),
(12, 1, b'0', 0, b'0'),
(13, 2, b'0', 0, b'0'),
(14, 2, b'0', 0, b'0'),
(15, 2, b'0', 0, b'1'),
(16, 2, b'0', 0, b'1'),
(17, 3, b'0', 0, b'1'),
(18, 3, b'0', 0, b'1'),
(19, 3, b'0', 0, b'1'),
(20, 3, b'1', 0, b'1'),
(21, 3, b'0', 0, b'1'),
(22, 4, b'0', 0, b'1'),
(23, 4, b'0', 0, b'1'),
(24, 5, b'0', 0, b'1'),
(25, 6, b'1', 0, b'1'),
(26, 6, b'0', 0, b'1'),
(27, 6, b'0', 0, b'1'),
(28, 6, b'0', 0, b'1'),
(29, 6, b'0', 0, b'1'),
(30, 6, b'0', 0, b'1'),
(31, 7, b'1', 0, b'1'),
(32, 7, b'0', 0, b'1'),
(33, 8, b'0', 0, b'1'),
(34, 8, b'1', 0, b'1'),
(35, 8, b'0', 0, b'1'),
(36, 9, b'0', 0, b'1'),
(37, 9, b'0', 0, b'1'),
(38, 10, b'0', 0, b'0'),
(39, 10, b'0', 0, b'0'),
(40, 10, b'0', 0, b'0'),
(41, 10, b'0', 0, b'0'),
(42, 16, b'0', 0, b'1'),
(43, 16, b'0', 0, b'1'),
(44, 16, b'0', 0, b'1'),
(45, 17, b'0', 0, b'1'),
(46, 17, b'0', 0, b'1'),
(47, 18, b'0', 0, b'1'),
(48, 18, b'0', 0, b'1'),
(49, 18, b'1', 0, b'1'),
(50, 19, b'0', 0, b'1'),
(51, 19, b'0', 0, b'1'),
(52, 19, b'0', 0, b'1'),
(53, 19, b'0', 0, b'1'),
(54, 19, b'0', 0, b'1'),
(55, 20, b'0', 0, b'1'),
(56, 20, b'0', 0, b'1'),
(57, 26, b'0', 0, b'1'),
(58, 26, b'0', 0, b'1'),
(59, 26, b'0', 0, b'1'),
(60, 27, b'0', 0, b'1'),
(61, 27, b'0', 0, b'1'),
(62, 28, b'0', 0, b'1'),
(63, 29, b'0', 0, b'1'),
(64, 29, b'1', 0, b'1'),
(65, 29, b'0', 0, b'1'),
(66, 29, b'0', 0, b'1'),
(67, 30, b'0', 0, b'1'),
(68, 30, b'0', 0, b'1'),
(69, 31, b'0', 0, b'1'),
(70, 31, b'0', 0, b'1'),
(71, 31, b'0', 0, b'1'),
(72, 32, b'0', 0, b'1'),
(73, 32, b'0', 0, b'1'),
(74, 33, b'0', 0, b'1'),
(75, 33, b'0', 0, b'1'),
(76, 33, b'1', 0, b'1'),
(77, 33, b'0', 0, b'1'),
(78, 33, b'0', 0, b'1'),
(79, 34, b'0', 0, b'1'),
(80, 34, b'0', 0, b'1'),
(81, 34, b'0', 0, b'1'),
(82, 35, b'0', 0, b'1'),
(83, 41, b'0', 0, b'1'),
(84, 41, b'0', 0, b'1'),
(85, 41, b'0', 0, b'1'),
(86, 41, b'0', 0, b'1'),
(91, 42, b'1', 0, b'1'),
(92, 42, b'0', 0, b'1'),
(93, 42, b'0', 0, b'1'),
(94, 42, b'0', 0, b'1'),
(95, 43, b'0', 0, b'1'),
(96, 43, b'0', 0, b'1'),
(97, 44, b'0', 0, b'1'),
(98, 45, b'0', 0, b'1'),
(99, 45, b'0', 0, b'1'),
(100, 45, b'0', 0, b'1'),
(101, 45, b'0', 0, b'1'),
(102, 46, b'0', 0, b'1'),
(103, 46, b'1', 0, b'1'),
(104, 46, b'0', 0, b'1'),
(105, 46, b'0', 0, b'1'),
(106, 47, b'0', 0, b'1'),
(107, 47, b'0', 0, b'1'),
(108, 47, b'0', 0, b'1'),
(109, 47, b'0', 0, b'1'),
(110, 47, b'0', 0, b'1'),
(111, 48, b'1', 0, b'1'),
(112, 48, b'0', 0, b'1'),
(113, 49, b'0', 0, b'1'),
(114, 50, b'0', 0, b'1'),
(115, 50, b'0', 0, b'1');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `descriptions`
--

CREATE TABLE `descriptions` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `author_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  `publisher_id` int(11) NOT NULL,
  `state` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `descriptions`
--

INSERT INTO `descriptions` (`id`, `title`, `author_id`, `genre_id`, `publisher_id`, `state`) VALUES
(1, 'List Ojca Świętego Jana Pawła II do artystów', 2, 1, 1, 0),
(2, 'Encykliki Ojca Świętego Jana Pawła II', 2, 1, 2, 0),
(3, 'List Ojca Świętego Jana Pawła II do kapłanów na Wielki Czwartek 1991 roku', 2, 1, 1, 0),
(4, 'Orędzie Ojca Świętego Jana Pawła II na Światowy Dzień Pokoju 1 stycznia 2001 roku', 2, 1, 1, 0),
(5, 'List Ojca Świętego Jana Pawła II do kobiet', 2, 1, 1, 0),
(6, 'Katarynka', 1, 2, 3, 0),
(7, 'Opowiadania wieczorne', 1, 2, 4, 0),
(8, 'Placówka', 1, 3, 4, 0),
(9, 'Lalka', 1, 3, 5, 0),
(10, 'Emancypantki', 1, 3, 3, 0),
(16, 'Wierna rzeka', 3, 3, 6, 0),
(17, 'Promień', 3, 3, 4, 0),
(18, 'Duma o hetmanie', 3, 4, 7, 0),
(19, 'Sen o szpadzie i Sen o chlebie', 3, 5, 8, 0),
(20, 'Wisła', 3, 4, 9, 0),
(26, 'To', 4, 6, 10, 0),
(27, 'Węgry', 4, 6, 11, 0),
(28, 'Zdobycie władzy', 4, 3, 12, 0),
(29, 'Jak powinno być w niebie', 4, 6, 10, 0),
(30, 'Wilno Jerozolimą było', 4, 6, 13, 0),
(31, 'Pan Tadeusz czyli Ostatni zajazd na Litwie', 5, 6, 14, 0),
(32, 'Księgi narodu polskiego i pielgrzymstwa polskiego', 5, 7, 15, 0),
(33, 'Grażyna : powieść litewska', 5, 6, 16, 0),
(34, 'Dziady', 5, 8, 16, 0),
(35, 'Trybuna Ludów', 5, 7, 16, 0),
(41, 'Lilla Weneda : tragedia w pięciu aktach', 6, 8, 17, 0),
(42, 'Wiersze i poematy', 6, 6, 3, 0),
(43, 'Podróż na Wschód', 6, 6, 18, 0),
(44, 'Fantazy', 6, 8, 19, 0),
(45, 'Listy do matki', 6, 6, 19, 0),
(46, 'Potop', 7, 3, 3, 0),
(47, 'Pisma zapomniane i niewydane', 7, 3, 20, 0),
(48, 'Latarnik', 7, 2, 21, 0),
(49, 'Pan Wołodyjowski', 7, 3, 23, 0),
(50, 'Quo vadis', 7, 3, 22, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `favorites`
--

CREATE TABLE `favorites` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `description_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `favorites`
--

INSERT INTO `favorites` (`id`, `user_id`, `description_id`) VALUES
(1, 1, 1),
(3, 1, 17),
(4, 1, 20),
(5, 1, 28),
(2, 1, 50);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `genres`
--

CREATE TABLE `genres` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `genres`
--

INSERT INTO `genres` (`id`, `name`) VALUES
(1, 'Życie religijne'),
(2, 'Nowela'),
(3, 'Powieść'),
(4, 'Poemat'),
(5, 'Proza'),
(6, 'Poezja'),
(7, 'Publicystyka'),
(8, 'Dramat');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(21);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `loans`
--

CREATE TABLE `loans` (
  `id` int(11) NOT NULL,
  `copy_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `return_date` date NOT NULL,
  `loan_state_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `loans`
--

INSERT INTO `loans` (`id`, `copy_id`, `user_id`, `created`, `return_date`, `loan_state_id`) VALUES
(1, 6, 1, '2019-09-30 22:00:00', '2019-11-30', 0),
(2, 16, 1, '2019-10-15 22:00:00', '2019-12-25', 0),
(3, 34, 1, '2019-10-03 22:00:00', '2019-10-30', 1),
(4, 19, 1, '2019-10-24 22:00:00', '2019-10-31', 1),
(5, 20, 1, '2019-10-20 22:00:00', '2019-10-26', 1),
(6, 22, 1, '2019-10-21 22:00:00', '2019-10-31', 2),
(7, 44, 1, '2019-09-30 22:00:00', '2019-10-29', 2),
(8, 25, 1, '2019-10-01 22:00:00', '2019-10-25', 2),
(9, 9, 2, '2019-12-25 16:20:32', '2020-01-01', 0),
(10, 10, 2, '2019-12-25 16:22:08', '2020-01-01', 0),
(11, 38, 2, '2019-12-25 16:35:49', '2020-01-01', 0),
(12, 39, 2, '2019-12-25 16:36:56', '2020-01-01', 0),
(13, 40, 2, '2019-12-25 16:36:57', '2020-01-01', 0),
(14, 41, 2, '2019-12-25 16:36:58', '2020-01-01', 0),
(15, 11, 2, '2019-12-25 16:37:01', '2020-01-01', 0),
(16, 12, 2, '2019-12-25 16:37:03', '2020-01-01', 0),
(19, 13, 2, '2019-12-25 16:45:45', '2020-01-01', 0),
(20, 14, 2, '2019-12-25 16:47:32', '2020-01-01', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `loan_states`
--

CREATE TABLE `loan_states` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci COMMENT='wypożyczona lub zarezerwowana lub oddana';

--
-- Zrzut danych tabeli `loan_states`
--

INSERT INTO `loan_states` (`id`, `name`) VALUES
(0, 'Zarezerwowana'),
(1, 'Wypożyczona'),
(2, 'Oddana');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `publishers`
--

CREATE TABLE `publishers` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `publishers`
--

INSERT INTO `publishers` (`id`, `name`) VALUES
(1, 'Watykan : [s. n.], (Watykan : Drukarnia Watykańska)'),
(2, 'Kraków : Dom Wydawniczy \"Rafael\" : Wydawnictwo AA'),
(3, 'Warszawa : Państ. Instytut Wydawniczy'),
(4, 'Warszawa : Gebethner i Wolff'),
(5, 'Kraków : Wydawnictwo Zielona Sowa'),
(6, 'Warszawa : Książka i Wiedza'),
(7, 'Warszawskich Pomocników Księgarskich'),
(8, 'Zakopane : nakładem Księgarni Podhalańskiej'),
(9, 'Warszawa ; Kraków : Towarzystwo Wydawnicze'),
(10, 'Kraków : \"Znak\"'),
(11, 'Paryż : Instytut Literacki'),
(12, 'Warszawa : Świat Książki'),
(13, 'Sejny : \"Pogranicze\"'),
(14, 'Kraków : nakł. Krakowskiej Spółki Wydawniczej,'),
(15, 'Paryż : Wydawnictwo Muzeum Adama Mickiewicza'),
(16, 'Warszawa : Czytelnik'),
(17, 'Kraków : Wydawnictwo M. Kot'),
(18, 'Jerozolima : Ministerstwo Wyznań Religijnych i Oświecenia Publicznego'),
(19, 'Wrocław : Wydawnictwo Zakładu Narodowego im. Ossolińskich'),
(20, 'Lwów : Drukarnia Zakładu Narodowego im. Ossolińskich'),
(21, 'Warszawa : Iskry'),
(22, 'Berlin : Schreitersche Verlagsbuchhandlung'),
(23, 'Warszawa : Redakcja \"Tygodnika Ilustrowanego\"');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ratings`
--

CREATE TABLE `ratings` (
  `id` int(11) NOT NULL,
  `description_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_negative` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ratings`
--

INSERT INTO `ratings` (`id`, `description_id`, `user_id`, `is_negative`) VALUES
(1, 1, 1, b'0'),
(2, 7, 2, b'1');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(20) COLLATE utf8_polish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `is_admin` bit(1) NOT NULL,
  `security_question` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `security_answer` varchar(100) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `email`, `is_admin`, `security_question`, `security_answer`) VALUES
(1, 'admin', '$2a$12$KTKGy9ps5l5Dmt3nHuLqC.pQagGAWsmVIXa9HTtdeUXcnwAINy3dy', 'admin@admin.pl', b'1', 'Jak mam na imie?', 'admin'),
(2, 'gracjan', '$2a$12$71cZ01h/ieRkJY7AMrXIYODNz875d/1rlAHnywXGR8FBDcZYXVnny', 'newEmail3', b'0', 'Jak mam na imie?', 'gracjan'),
(3, 'piotr', '$2a$12$zNTIcg0NVDfWMgYCUB3fQeVwMOzm0up8N1iZjrXcM82bf6OUoo4Ya', 'tykwe@mail.com', b'0', 'Jak mam na imie?', 'piotr'),
(4, 'dominik', '$2a$12$XyHF3fjb1/CQjIa3DLO6hOhv4JUxitzUhIes08JToATcLO4wCiSLC', 'newEmail', b'0', 'Jak mam na imie?', 'dominik');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_fk` (`user_id`),
  ADD KEY `comments_fk_1` (`description_id`);

--
-- Indeksy dla tabeli `copies`
--
ALTER TABLE `copies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `copies_fk` (`description_id`);

--
-- Indeksy dla tabeli `descriptions`
--
ALTER TABLE `descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `descriptions_fk` (`author_id`),
  ADD KEY `descriptions_fk_1` (`genre_id`),
  ADD KEY `descriptions_fk_2` (`publisher_id`);

--
-- Indeksy dla tabeli `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `favorites_un` (`user_id`,`description_id`),
  ADD KEY `favorites_fk` (`user_id`),
  ADD KEY `favorites_fk_1` (`description_id`);

--
-- Indeksy dla tabeli `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `loans`
--
ALTER TABLE `loans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `loans_fk` (`user_id`),
  ADD KEY `loans_fk_1` (`loan_state_id`),
  ADD KEY `loans_fk_2` (`copy_id`);

--
-- Indeksy dla tabeli `loan_states`
--
ALTER TABLE `loan_states`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `publishers`
--
ALTER TABLE `publishers`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ratings_un` (`description_id`,`user_id`),
  ADD KEY `likes_fk_1` (`user_id`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_sx468g52bpetvlad2j9y0lptc` (`login`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `authors`
--
ALTER TABLE `authors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `copies`
--
ALTER TABLE `copies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT dla tabeli `descriptions`
--
ALTER TABLE `descriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT dla tabeli `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `genres`
--
ALTER TABLE `genres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `loans`
--
ALTER TABLE `loans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT dla tabeli `loan_states`
--
ALTER TABLE `loan_states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `publishers`
--
ALTER TABLE `publishers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT dla tabeli `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_fk_1` FOREIGN KEY (`description_id`) REFERENCES `descriptions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `copies`
--
ALTER TABLE `copies`
  ADD CONSTRAINT `copies_fk` FOREIGN KEY (`description_id`) REFERENCES `descriptions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `descriptions`
--
ALTER TABLE `descriptions`
  ADD CONSTRAINT `descriptions_fk` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `descriptions_fk_1` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `descriptions_fk_2` FOREIGN KEY (`publisher_id`) REFERENCES `publishers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `favorites`
--
ALTER TABLE `favorites`
  ADD CONSTRAINT `favorites_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `favorites_fk_1` FOREIGN KEY (`description_id`) REFERENCES `descriptions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `loans`
--
ALTER TABLE `loans`
  ADD CONSTRAINT `loans_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `loans_fk_1` FOREIGN KEY (`loan_state_id`) REFERENCES `loan_states` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `loans_fk_2` FOREIGN KEY (`copy_id`) REFERENCES `copies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `ratings`
--
ALTER TABLE `ratings`
  ADD CONSTRAINT `likes_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ratings_fk` FOREIGN KEY (`description_id`) REFERENCES `descriptions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
